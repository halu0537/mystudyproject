//
//  DefaultServiceLocator.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 17.03.2021.
//

class DefaultServiceLocator: ServiceLocator {
    var userService = DefaultUserService()

    lazy var mainService = MainServiceImpl(userService: userService)
}
