//
//  MainService.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 17.03.2021.
//

// MARK: - Service
class MainServiceImpl {
    var userService: DefaultUserService

    private let requestManager = RequestManager()

    init(userService: DefaultUserService) {
        self.userService = userService
    }
}

// MARK: - Functions
extension MainServiceImpl { }
