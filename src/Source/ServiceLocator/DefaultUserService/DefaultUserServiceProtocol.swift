//
//  DefaultUserServiceProtocol.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 17.03.2021.
//

protocol DefaultUserServiceProtocol {
    var wasLaunch: Bool { get }
    func didLaunch(complection: (() -> Void)?)

    // MARK: - User Is Authorized
    var userIsAuthorized: Bool { get }
    func userAuthorizationCheck(userIsAuthorized: Bool)
}
