//
//  DefaultUserService.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 17.03.2021.
//

import Foundation

class DefaultUserService: DefaultUserServiceProtocol {
    // MARK: - First Launch
    private let wasLaunchKey = "wasLaunchKey"
    lazy var wasLaunch: Bool = {
        return getBool(key: wasLaunchKey) ?? false// == nil ? true : getBool(key: isFirstLaunchKey)

    }()
    func didLaunch(complection: (() -> Void)?) {
        setValue(true, key: wasLaunchKey)
        complection?()
    }

    // MARK: - User Is Authorized
     private let userIsAuthorizedKey = "userIsAuthorizedKey"
     lazy var userIsAuthorized: Bool = {
        getBool(key: userIsAuthorizedKey) ?? false
     }()
     func userAuthorizationCheck(userIsAuthorized: Bool) {
         setValue(userIsAuthorized, key: userIsAuthorizedKey)
     }

    // MARK: - Work with UserDefaults
    private func setValue(_ value: Any?, key: String) {
        UserDefaults.standard.set(value, forKey: key)
        UserDefaults.standard.synchronize()
    }

//    private func setImage(_ image: UIImage, key: String) {
//        let value = image.pngData()
//        UserDefaults.standard.set(value, forKey: key)
//        UserDefaults.standard.synchronize()
//    }

    private func getString(key: String) -> String? {
        let string = UserDefaults.standard.string(forKey: key)
        UserDefaults.standard.synchronize()
        return string
    }

    private func getBool(key: String) -> Bool? {
        let bool = UserDefaults.standard.bool(forKey: key)
        UserDefaults.standard.synchronize()
        return bool
    }
}
