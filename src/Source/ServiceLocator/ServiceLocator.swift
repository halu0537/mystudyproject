//
//  ServiceLocator.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 17.03.2021.
//

protocol ServiceLocator {
    var userService: DefaultUserService { get }

    var mainService: MainServiceImpl { get }
}
