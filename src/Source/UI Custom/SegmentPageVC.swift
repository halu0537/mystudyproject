//
//  SegmentPageVC.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 20.03.2021.
//

//    import UIKit
//
//    class SegmentPageVC: UIPageViewController {
//
//        private var currentViewController: UIViewController?
//        private var currentIndex = 0
//
//        // MARK: - create vc
//        private var arrayViewController = [UIViewController]()
//        private var sgControll: CustomSegmentedControl?
//
//        // MARK: - init UIPageViewController
//        init(transitionStyle style: UIPageViewController.TransitionStyle,
//                      navigationOrientation: UIPageViewController.NavigationOrientation,
//                      options: [String : Any]? = nil) {
//            super.init(transitionStyle: style, navigationOrientation: navigationOrientation, options: nil)
//
//            self.dataSource = self
//            self.delegate = self
//
//            self.view.backgroundColor = UIColor.init(red: 0.94, green: 0.94, blue: 0.96, alpha: 1)
//
//    //        if let vc = arrayViewController.first {
//    //            setViewControllers([vc], direction: .forward, animated: true, completion: nil)
//    //            self.currentViewController = vc
//    //        }
//        }
//
//        required init?(coder: NSCoder) {
//            fatalError("init(coder:) has not been implemented")
//        }
//
//        func setVCs(VCs: [UIViewController], segmentController: CustomSegmentedControl, titles: [String]) {
//            self.arrayViewController = VCs
//            if let vc = arrayViewController.first {
//                setViewControllers([vc], direction: .forward, animated: true, completion: nil)
//                self.currentViewController = vc
//                self.currentIndex = 0
//            }
//
//            self.sgControll = segmentController
//            segmentController.addTarget(self, action: #selector(segmentedControlValueChanged), for: .valueChanged)
//
//            for i in 0 ..< titles.count {
//                segmentController.insertSegment(withTitle: titles[i], at: i, animated: false)
//                if i == titles.count - 1 {
//                    segmentController.selectedSegmentIndex = 0
//                    segmentController.addUnderlineForSelectedSegment()
//                }
//            }
//        }
//
//        @objc private func segmentedControlValueChanged(_ sender: UISegmentedControl) {
//            let index = sender.selectedSegmentIndex
//            if index < self.arrayViewController.count {
//                let vc = arrayViewController[index]
//                let direction: UIPageViewController.NavigationDirection =
// index > self.currentIndex
// ? .forward : .reverse
//                setViewControllers([vc], direction: direction, animated: true, completion: nil)
//                self.currentViewController = vc
//                self.currentIndex = index
//                self.sgControll?.changeUnderlinePosition()
//            }
//        }
//
//    }
//
//    // MARK: - UIPageViewControllerDelegate
//    extension SegmentPageVC: UIPageViewControllerDelegate {
//        func pageViewController(_ pageViewController: UIPageViewController,
//            didFinishAnimating finished: Bool,
//            previousViewControllers: [UIViewController],
//            transitionCompleted completed: Bool) {
//            guard completed else { return }
//            if let vc = pageViewController.viewControllers?.first,
// let index = arrayViewController.firstIndex(of: vc) {
//                self.currentIndex = index
//                self.currentViewController = vc
//                self.sgControll?.selectedSegmentIndex = index
//                self.sgControll?.changeUnderlinePosition()
//            }
//        }
//    }
//
//    // MARK: - UIPageViewControllerDataSource
//    extension SegmentPageVC: UIPageViewControllerDataSource {
//        func pageViewController(_ pageViewController: UIPageViewController,
// viewControllerBefore viewController: UIViewController) -> UIViewController? {
//            if let index = arrayViewController.firstIndex(of: viewController),
//                index > 0 {
//                return arrayViewController[index - 1]
//            }
//            return nil
//        }
//
//        func pageViewController(_ pageViewController: UIPageViewController,
// viewControllerAfter viewController: UIViewController) -> UIViewController? {
//            if let index = arrayViewController.firstIndex(of: viewController),
//                index < arrayViewController.count - 1 {
//                return arrayViewController[index + 1]
//            }
//            return nil
//        }
//    }
//
//    // MARK: - Constants
//    extension SegmentPageVC {
//        private enum Constants {}
//    }
//
//    // MARK: - Extension UISegmentedControl
//    extension UISegmentedControl {
//        func removeBorder(){
//            let backgroundImage = UIImage.getColoredRectImageWith(color: UIColor.white.cgColor,
//    andSize: self.bounds.size)
//            self.setBackgroundImage(backgroundImage, for: .normal, barMetrics: .default)
//            self.setBackgroundImage(backgroundImage, for: .selected, barMetrics: .default)
//            self.setBackgroundImage(backgroundImage, for: .highlighted, barMetrics: .default)
//
//            let deviderImage = UIImage.getColoredRectImageWith(color: UIColor.white.cgColor,
//   andSize: CGSize(width: 1.0, height: self.bounds.size.height))
//            self.setDividerImage(deviderImage, forLeftSegmentState: .selected,
// rightSegmentState: .normal, barMetrics: .default)
//        }
//
//        func addUnderlineForSelectedSegment(){
//            removeBorder()
//            let underlineWidth: CGFloat = self.bounds.size.width / CGFloat(self.numberOfSegments)
//            let underlineHeight: CGFloat = 3.0
//            let underlineXPosition = CGFloat(selectedSegmentIndex * Int(underlineWidth))
//            let underLineYPosition = self.bounds.size.height - 3.0
//            let underlineFrame = CGRect(x: underlineXPosition + 5, y: underLineYPosition,
// width: underlineWidth - 10, height: underlineHeight)
//            let underline = UIView(frame: underlineFrame)
//            underline.backgroundColor = UIColor.init(red: 0.431, green: 0.404, blue: 0.996, alpha: 1)
//            underline.tag = 1
//            self.addSubview(underline)
//        }
//
//        func changeUnderlinePosition(){
//            guard let underline = self.viewWithTag(1) else {return}
//            let underlineFinalXPosition = (self.bounds.width /
// CGFloat(self.numberOfSegments)) * CGFloat(selectedSegmentIndex)
//            UIView.animate(withDuration: 0.1, animations: {
//                underline.frame.origin.x = underlineFinalXPosition + 5
//
//            })
//        }
//    }
//    // MARK: - Extension UIImage
//    extension UIImage {
//        class func getColoredRectImageWith(color: CGColor, andSize size: CGSize) -> UIImage{
//            UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
//            let graphicsContext = UIGraphicsGetCurrentContext()
//            graphicsContext?.setFillColor(color)
//            let rectangle = CGRect(x: 0.0, y: 0.0, width: size.width, height: size.height)
//            graphicsContext?.fill(rectangle)
//            let rectangleImage = UIGraphicsGetImageFromCurrentImageContext()
//            UIGraphicsEndImageContext()
//            return rectangleImage ?? UIImage()
//        }
//    }
