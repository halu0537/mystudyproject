//
//  CustomTitleLabel.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 20.03.2021.
//

import UIKit

class CustomTitleLabel: UILabel {
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.textColor = UIColor(hex: "#2128BDFF")
//         UIColor(red: 0.129, green: 0.157, blue: 0.741, alpha: 1)
//        self.font = UIFont.init(name: CustomFonts.sFProDisplayRegular, size: 28)
        self.numberOfLines = 0
        self.adjustsFontSizeToFitWidth = true
        self.translatesAutoresizingMaskIntoConstraints = false

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineHeightMultiple = CGFloat(1.01)
        self.attributedText = NSMutableAttributedString(
            string: "",
            attributes: [
                NSAttributedString.Key.kern: 0.37,
                NSAttributedString.Key.paragraphStyle: paragraphStyle
            ]
        )
        self.textAlignment = .center
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
