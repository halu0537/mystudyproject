//
//  WhiteNavigationBar.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 20.03.2021.
//

import UIKit

open class NavigationBar: UINavigationBar, DesignSystemElement {
    // MARK: - Enums
    private enum Styles: String {
        case whiteNavigationBar
    }

    @objc dynamic public var theme: Theme? {
        didSet {
            refreshUI()
        }
    }

    public var styleRaw: String?

    public override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }

    public init() {
        super.init(frame: .zero)
        setupViews()
    }

    public required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupViews()
    }

    open func refreshUI() {
        barTintColor = styleAttributes?.backgroundColor
        tintColor = styleAttributes?.tintColor
        var styles = [NSAttributedString.Key: Any]()
        if let font = styleAttributes?.accessibilityFont {
            styles[.font] = font
        }
        if let textColor = styleAttributes?.textColor {
            styles[.foregroundColor] = textColor
        }
        titleTextAttributes = styles
        shadowImage = UIImage()
        isTranslucent = false
    }

    private func setupViews() {
        applyStyle(style: Styles.whiteNavigationBar.rawValue)
        shadowImage = UIImage()
        isTranslucent = false
    }
}
