//
//  MainButton.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 21.03.2021.
//

import UIKit

/**
Для девайсов с челкой всегда отступ всегда бдует большой (увеличивается на safearea).
Если этот отступ не нужен, например, кнопка испльзуется где-то в середине экрана,
нужно простаить insetsLayoutMarginsFromSafeArea = false
*/

public class MainButton: UIView {
    // MARK: - Private props
    private var contentInsets = UIEdgeInsets(top: 12, left: 16, bottom: 18, right: 16)

    // MARK: - Views
    private var button: MainButtonElement!
    private let backgroundView = MainButtonBackgroundView(frame: .zero)

    // MARK: - Callback
    public var didTap: (() -> Void)?

    // MARK: - Initialization
    public init() {
        super.init(frame: .zero)
        addSubviews()
        configureUI()
    }

    @available(*, unavailable)
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Public interface
    public func setTitle(_ title: String?) {
        button.setTitle(title, for: .normal)
    }

    public var isEnabled: Bool {
        get {
            button.isEnabled
        }
        set {
            button.isEnabled = newValue
        }
    }
    public var isBackgroundViewHidden: Bool {
        get {
            backgroundView.isHidden
        }
        set {
            backgroundView.isHidden = newValue
        }
    }

    public func setAccessibilityLabel(_ accessibilityLabel: String?) {
        button.isAccessibilityElement = true
        button.accessibilityLabel = accessibilityLabel
    }

    // MARK: - Private interface
    private func addSubviews() {
        addPrepareSubview(backgroundView)
        backgroundView.snp.makeConstraints {
            $0.top.leading.trailing.bottom.equalToSuperview()
        }

        button = MainButtonElement()
        addPrepareSubview(button)
        button.snp.makeConstraints {
            $0.top.equalToSuperview().offset(contentInsets.top)
            $0.leading.equalToSuperview().offset(contentInsets.left)
            $0.trailing.equalToSuperview().offset(-contentInsets.right)
            $0.bottom.equalToSuperview().offset(-contentInsets.bottom)
            $0.height.equalTo(50)
        }
    }

    private func configureUI() {
        button.addTarget(self, action: #selector(didTapButton), for: .touchUpInside)
    }

    @objc private func didTapButton() {
        didTap?()
    }

    // MARK: - Intrinsic Content Size

    public override var intrinsicContentSize: CGSize {
        return CGSize(width: 250, height: 80)
    }
}

class MainButtonElement: VTBButton {
    // MARK: - Enums
    private enum Styles: String {
        case mainButtonHighlighted
        case mainButtonDisabled
        case mainButton
    }

    public override var isHighlighted: Bool {
        didSet {
            if isHighlighted {
                applyStyle(style: Styles.mainButtonHighlighted.rawValue)
            } else {
                applyStyle(style: Styles.mainButton.rawValue)
            }
        }
    }

    public override var isEnabled: Bool {
        didSet {
            if isEnabled {
                applyStyle(style: Styles.mainButton.rawValue)
            } else {
                applyStyle(style: Styles.mainButtonDisabled.rawValue)
            }
        }
    }

    // MARK: - Initialization

    public required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    public init() {
        super.init(frame: .zero)
        setupViews()
    }

    // MARK: - Setup

    private func setupViews() {
        applyStyle(style: Styles.mainButton.rawValue)
        layer.cornerRadius = 8
    }
}

open class VTBButton: UIButton, DesignSystemElement {
    @objc dynamic public var theme: Theme? {
        didSet {
            refreshUI()
        }
    }
    /// A Boolean value indicating whether the control is in the selected state.
    open override var isSelected: Bool {
        didSet { configureImage(for: state) }
    }
    /// A Boolean value indicating whether the control draws a highlight.
    open override var isHighlighted: Bool {
        didSet { configureImage(for: state) }
    }
    /// A Boolean value indicating whether the control is in the enabled state.
    open override var isEnabled: Bool {
        didSet { configureImage(for: state) }
    }

    public var styleRaw: String?
    public var isDynamicFontDisabled = false
    private var darkImageParameter = VTBButtonParameter<UIImage?>() {
        didSet { configureImage(for: state) }
    }
    private var lightImageParameter = VTBButtonParameter<UIImage?>() {
        didSet { configureImage(for: state) }
    }
    open override func setImage(_ image: UIImage?, for state: UIControl.State) {
        lightImageParameter.set(parameter: image, for: state)
        configureImage(for: state)
    }
    open func setDarkImage(_ image: UIImage?, for state: UIControl.State) {
        darkImageParameter.set(parameter: image, for: state)
        configureImage(for: state)
    }

    open override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        guard previousTraitCollection?.preferredContentSizeCategory !=
                traitCollection.preferredContentSizeCategory else {
            return
        }
        refreshUI()
    }

    open func refreshUI() {
        if let tintColor = styleAttributes?.tintColor {
            self.tintColor = tintColor
        }
        setTitleColor(styleAttributes?.textColor, for: .normal)
        backgroundColor = styleAttributes?.backgroundColor ?? .clear
        setTitleColor(styleAttributes?.disabledColor, for: .disabled)
        titleLabel?.font = isDynamicFontDisabled ? styleAttributes?.font : styleAttributes?.accessibilityFont
        layer.borderColor = styleAttributes?.borderColor?.cgColor ?? UIColor.clear.cgColor
        configureImage(for: state)
    }
    func configureImage(for state: UIControl.State) {
        let isDark = theme?.isDark ?? false
        if isDark {
            let darkImage = darkImageParameter.parameter(for: state) as? UIImage
            let lightImage = lightImageParameter.parameter(for: state) as? UIImage
            super.setImage(darkImage ?? lightImage, for: state)
        } else {
            super.setImage(lightImageParameter.parameter(for: state) as? UIImage, for: state)
        }
    }
}

private extension StyleAttributes {
    var borderColor: UIColor? { return colors["borderColor"] }
}

struct VTBButtonParameter<Parameter> {
    private var parameters: [UInt: Parameter] = [:]
    init() {
        //
    }
    mutating func set(parameter: Parameter?, for state: UIControl.State) {
        self.parameters[state.rawValue] = parameter
    }

    func parameter(for state: UIControl.State) -> Parameter? {
        if let result = parameters[state.rawValue] ?? parameters[UIControl.State.normal.rawValue] {
            return result
        } else {
            return nil
        }
    }
    func forEachState(_ closure: ((UIControl.State, Parameter) -> Void)) {
        parameters.forEach({
            closure(UIControl.State(rawValue: $0.key), $0.value)
        })
    }
}

class MainButtonBackgroundView: CustomView {
    private enum Styles: String {
        case mainButtonTransparentBackground
    }

    private let blurView = UIVisualEffectView()
    private let transparentView = CustomView()

    override init(frame: CGRect) {
        super.init(frame: frame)

        setupSubviews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func refreshUI() {
        super.refreshUI()

        applyBackgroundEffect()
    }

    private func setupSubviews() {
        addPrepareSubview(blurView)
        blurView.effect = UIBlurEffect(style: .light)
        blurView.snp.makeConstraints {
            $0.top.leading.trailing.bottom.equalToSuperview()
        }

        addPrepareSubview(transparentView)
        transparentView.snp.makeConstraints {
            $0.top.leading.trailing.bottom.equalToSuperview()
        }
        transparentView.applyStyle(style: Styles.mainButtonTransparentBackground.rawValue)
        transparentView.alpha = 0.5

        applyBackgroundEffect()
    }

    private func applyBackgroundEffect() {
        let isDark = DesignManager.shared.currentTheme?.isDark ?? false
        blurView.isHidden = isDark
        transparentView.isHidden = !isDark
    }
}
