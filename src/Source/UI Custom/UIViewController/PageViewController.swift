//
//  PageViewController.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 20.03.2021.
//

//    import UIKit
//
//    class PageViewController: UIPageViewController {
//        
//        let rootView = CustomView()
//        
//        override init(transitionStyle style: UIPageViewController.TransitionStyle,
// navigationOrientation: UIPageViewController.NavigationOrientation,
// options: [UIPageViewController.OptionsKey : Any]? = nil) {
//            super.init(transitionStyle: style, navigationOrientation: navigationOrientation, options: options)
//            rootView.styleRaw = "rootViewController"
//            view.addSubview(rootView)
//            
//            rootView.snp.makeConstraints {
//                $0.top.leading.trailing.bottom.equalToSuperview()
//            }
//        }
//        
//        required init?(coder: NSCoder) {
//            fatalError("init(coder:) has not been implemented")
//        }
//    }
