//
//  CustomView.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 20.03.2021.
//

import UIKit

open class CustomView: UIView, DesignSystemElement {
    @objc dynamic public var theme: Theme? {
        didSet {
            refreshUI()
        }
    }

    public var styleRaw: String?

    open override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        guard previousTraitCollection != nil,
              previousTraitCollection?.preferredContentSizeCategory !=
                traitCollection.preferredContentSizeCategory else {
            return
        }
        refreshUI()
    }

    open func refreshUI() {
        if let backgroundColor = styleAttributes?.backgroundColor {
            self.backgroundColor = backgroundColor
        }
    }
}
