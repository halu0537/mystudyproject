//
//  ViewController.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 17.03.2021.
//

import UIKit

open class ViewController: UIViewController {
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        setNeedsStatusBarAppearanceUpdate()
    }

    required public init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    open override func loadView() {
        let rootView = CustomView()
        rootView.styleRaw = "rootViewController"
        view = rootView
    }

    @objc func statusBarChanged() {
        setNeedsStatusBarAppearanceUpdate()
    }

    open override var preferredStatusBarStyle: UIStatusBarStyle {
        let isDarkTheme = DesignManager.shared.currentTheme?.isDark ?? false
        if isDarkTheme {
            return .lightContent
        } else {
            if #available(iOS 13.0, *) {
                return .darkContent
            } else {
                return .default
            }
        }
    }
}
