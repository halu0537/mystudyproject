//
//  CustomBlueButton.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 20.03.2021.
//

import UIKit

class CustomBlueButton: UIButton {
    init(rightArrow: Bool) {
        super.init(frame: .zero)
        self.setup()
        if rightArrow {
            let imageView = UIImageView()
            imageView.image = UIImage(named: "rightArrowUICustom")
            imageView.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview(imageView)

            imageView.snp.makeConstraints { make in
                make.trailing.equalToSuperview().offset(-20)
                make.centerY.equalToSuperview()
                make.width.equalTo(12)
                make.height.equalTo(20)
            }
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    private func setup() {
//        self.titleLabel?.font = UIFont.init(name: CustomFonts.sFUITextSemibold, size: 17)
        self.setTitleColor(.white, for: .normal)
        self.setTitleColor(.lightGray, for: .highlighted)
        self.layer.cornerRadius = 14
        self.translatesAutoresizingMaskIntoConstraints = false
    }

    func setupGradient(bounds: CGRect, shadow: Bool) {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = bounds
        gradientLayer.cornerRadius = 14
        gradientLayer.colors = [
            UIColor(hex: "#B347FFff")?.cgColor as Any,
            UIColor(hex: "#6D67FEff")?.cgColor as Any
        ]
        gradientLayer.startPoint = CGPoint(x: 0.0, y: 0.5)
        gradientLayer.endPoint = CGPoint(x: 1.0, y: 0.5)
        self.layer.insertSublayer(gradientLayer, at: 0)

        if shadow {
            self.layer.shadowColor = UIColor.black.cgColor
            self.layer.shadowOpacity = 0.08
            self.layer.shadowOffset = CGSize(width: 0, height: 15)
        }
    }
}
