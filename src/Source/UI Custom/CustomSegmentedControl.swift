//
//  CustomSegmentedControl.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 20.03.2021.
//

//    import UIKit
//
//    class CustomSegmentedControl: UISegmentedControl {
//
//        private var segmentTitles = [String]()
//        
//        init(withUnderline: Bool) {
//            super.init(frame: CGRect())
//            self.backgroundColor = .clear
//            self.tintColor = .clear
//    //        UIImageView.appearance(whenContainedInInstancesOf: [UISegmentedControl.self]).backgroundColor = .white
//            
//            self.setTitleTextAttributes(
//                [
//                    NSAttributedString.Key.foregroundColor: UIColor.black],
//    //                NSAttributedString.Key.font: UIFont.init(
//    //                    name: CustomFonts.sFProTextMedium,
//    //                    size: 14
//    //                ) as Any],
//                for: .normal
//            )
//            
//            self.setTitleTextAttributes(
//                [ NSAttributedString.Key.foregroundColor: UIColor.init(red: 0.431, green: 0.404,
// blue: 0.996, alpha: 1)],
//    //            NSAttributedString.Key.font: UIFont.init(name: CustomFonts.sFUITextSemibold, size: 14) as Any],
//                                        for: .selected)
//        }
//        
//        init(segmentTitles: [String]) {
//            super.init(frame: CGRect())
//            self.backgroundColor = .white
//            self.tintColor = .white
//            self.segmentTitles = segmentTitles
//            self.setupSegments(newSegment: true)
//            self.addTarget(self, action: #selector(segmentedControlValueChanged), for: .valueChanged)
//            UIImageView.appearance(whenContainedInInstancesOf: [UISegmentedControl.self]).backgroundColor = .white
//            
//        }
//        
//        required init?(coder: NSCoder) {
//            fatalError("init(coder:) has not been implemented")
//        }
//        
//        private func setupSegments(newSegment: Bool) {
//            for i in 0 ..< self.segmentTitles.count {
//                
//                let isSelected = i == self.selectedSegmentIndex
//                let named = isSelected ? "selectedCircle" : "deselectedCircle"
//    //            let font = UIFont.init(name: CustomFonts.sFProDisplayRegular, size: 17)
//                guard let image = UIImage(named: named) else { return }
//                
//                let currentImage = self.textEmbeded(image: image,
//                                                    string: self.segmentTitles[i],
//                                                    isImageBeforeText: true,
//                                                    segFont: nil).withRenderingMode(.alwaysOriginal)
//                
//                if newSegment && i == 0, let image = UIImage(named: "selectedCircle") {
//                    let currentImage = self.textEmbeded(image: image,
//                                                        string: self.segmentTitles[i],
//                                                        isImageBeforeText: true,
//                                                        segFont: nil).withRenderingMode(.alwaysOriginal)
//                    self.insertSegment(with: currentImage, at: i, animated: false)
//                    self.selectedSegmentIndex = i
//                } else if newSegment {
//                    self.insertSegment(with: currentImage, at: i, animated: false)
//                } else {
//                    self.setImage(currentImage, forSegmentAt: i)
//                }
//            }
//        }
//        
//        @objc func segmentedControlValueChanged() {
//            self.setupSegments(newSegment: false)
//        }
//        
//        private func textEmbeded(image: UIImage,
//                               string: String,
//                    isImageBeforeText: Bool,
//                              segFont: UIFont? = nil) -> UIImage {
//            let font = segFont ?? UIFont.systemFont(ofSize: 16)
//            let expectedTextSize = (string as NSString).size(withAttributes: [.font: font])
//            let width = expectedTextSize.width + image.size.width + 5
//            let height = max(expectedTextSize.height, image.size.width)
//            let size = CGSize(width: width, height: height)
//            
//            let renderer = UIGraphicsImageRenderer(size: size)
//            return renderer.image { context in
//                let fontTopPosition: CGFloat = (height - expectedTextSize.height) / 2
//                let textOrigin: CGFloat = isImageBeforeText ? image.size.width + 5 : 0
//                let textPoint: CGPoint = CGPoint.init(x: textOrigin, y: fontTopPosition)
//                string.draw(at: textPoint, withAttributes: [.font: font])
//                let alignment: CGFloat = isImageBeforeText ? 0 : expectedTextSize.width + 5
//                let rect = CGRect(x: alignment,
//                                  y: (height - image.size.height) / 2,
//                              width: image.size.width,
//                             height: image.size.height)
//                image.draw(in: rect)
//            }
//        }
//    }
