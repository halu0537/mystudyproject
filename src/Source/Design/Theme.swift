//
//  Theme.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 20.03.2021.
//

import UIKit

public enum ThemeKey: String, CaseIterable {
    case light = "lightTheme"
    case dark = "darkTheme"
    case base = "baseTheme"
    case undefined = "undefined"
}

@objc public class Theme: NSObject {
    public let key: ThemeKey
    public let isDark: Bool
    public var styleAttributes: [String: StyleAttributes]

    init(key: ThemeKey, isDark: Bool, styleAttributes: [String: StyleAttributes]) {
        self.key = key
        self.isDark = isDark
        self.styleAttributes = styleAttributes
    }

    public func style(named: String) -> StyleAttributes? {
        return styleAttributes[named]
    }
}
