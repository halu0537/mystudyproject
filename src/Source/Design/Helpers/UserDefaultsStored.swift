//
//  UserDefaultsStored.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 20.03.2021.
//

import Foundation

@propertyWrapper
struct UserDefaultsStored<T> {
    private let key: String
    private let userDefaults: UserDefaults
    private let defaultValue: T

    init(key: String, defaultValue: T, userDefaults: UserDefaults? = nil) {
        self.key = key
        self.userDefaults = userDefaults ?? UserDefaults.standard
        self.defaultValue = defaultValue
    }

    var wrappedValue: T? {
        get {
            // Read value from UserDefaults
//            return (userDefaults.value(forKey: key) as? T) ?? defaultValue
            return "lightTheme" as? T
//            return "darkTheme" as! T
        }
        set {
            // Set value to UserDefaults
            userDefaults.set(newValue, forKey: key)
        }
    }
}
