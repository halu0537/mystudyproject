//
//  CurrentThemeDefault.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 20.03.2021.
//

import UIKit

extension Notification.Name {
    static let CurrentThemeDidChange = Notification.Name("CurrentThemeDidChange")
}

protocol CurrentTheme {
    static var current: ThemeKey { get set }
}

class CurrentThemeDefault: CurrentTheme {
    @UserDefaultsStored(
        key: "CurrentThemeDefault",
        defaultValue: ThemeKey.light.rawValue,
        userDefaults: UserDefaults.standard
    )
    private static var currentStoredString: String?
    private static var currentStored: ThemeKey {
        get {
            ThemeKey(rawValue: currentStoredString ?? "") ?? .light
        }
        set {
            currentStoredString = newValue.rawValue
        }
    }

    static var current: ThemeKey {
        get {
            currentStored
        }
        set {
            currentStored = newValue
            notifyThemeChanged()
        }
    }

    private static func notifyThemeChanged() {
        var userInfo: [String: ThemeKey]
        userInfo = ["current": Self.current]

        NotificationCenter.default.post(name: .CurrentThemeDidChange, object: nil, userInfo: userInfo)
    }
}
