//
//  DesignSystemElement.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 20.03.2021.
//

import UIKit

public protocol DesignSystemElement: UIView {
    var theme: Theme? { get set }
    var styleRaw: String? { get set }
    func refreshUI()
}

public extension DesignSystemElement {
    func applyStyle(style: String) {
        self.styleRaw = style
        refreshUI()
    }

    var styleAttributes: StyleAttributes? {
        guard let style = styleRaw else { return nil }
        let theme = DesignManager.shared.currentTheme?.style(named: style)
        return theme

//        return theme?.style(named: style)
    }

    /**
     Метод используется для того, чтобы зафорсить применение UIAppearance к вьюшке.
     В случае, если у вьюшки есть superview, метод ничего не делает, чтобы избежать побочных эффектов.

     Не удалось найти, в какой момент применяется UIAppearance к вьюшкам, но, похоже,
     что тогда, когда мы добавляем ее в иерархию на экран addSubview.
     При этом конечный superview должен быть keyWindow, т.е. вьюшка на самом деле показывается на экране.
     Поэтому для вьюшек, которые существуют только в памяти, но не на экране, можно вызывать этот метод,
     чтобы применить к ней UIAppearance.

     - SeeAlso: forceApplyUIAppearanceIfPossible(affectingSuperviews: Bool)
     */
    func forceApplyUIAppearanceIfPossible() {
        guard superview == nil else { return }

        UIApplication.shared.keyWindow?.addSubview(self)
        self.removeFromSuperview()
    }

    /**
     Метод используется для того, чтобы зафорсить применение UIAppearance к вьюшке.

     - Parameter affectingSuperviews: Определяет, нужно ли кратковременно добавить в иерархию
     только саму вьюшку, или же вьюшку в некотором контейнере из её супервьюшек.
     При `affectingSuperviews` == `false` метод аналогичен `forceApplyUIAppearanceIfPossible()`.
     При `affectingSuperviews` == `true` проходит вверх по иерархии супервьюшек и кратковременно удаляет
     и добавляет ближайшую, у которой `superview` == `nil`.

     - SeeAlso: forceApplyUIAppearanceIfPossible()
     */
    func forceApplyUIAppearanceIfPossible(affectingSuperviews: Bool) {
        if affectingSuperviews {
            forceApplyUIAppearanceAffectingSuperviews()
        } else {
            forceApplyUIAppearanceIfPossible()
        }
    }

    private func forceApplyUIAppearanceAffectingSuperviews() {
        guard let windowlessSuperviewOrSelf = firstWindowlessViewStarting(with: self) else { return }

        UIApplication.shared.keyWindow?.addSubview(windowlessSuperviewOrSelf)
        windowlessSuperviewOrSelf.removeFromSuperview()
    }

    private func firstWindowlessViewStarting(with view: UIView) -> UIView? {
        if view === UIApplication.shared.keyWindow {
            return nil
        }
        guard let superView = view.superview else {
            return view
        }
        return firstWindowlessViewStarting(with: superView)
    }
}
