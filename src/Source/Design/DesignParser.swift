//
//  DesignParser.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 20.03.2021.
//

// swiftlint:disable identifier_name
import UIKit

class DesignParser: Decodable {
    // MARK: - Private props
    private var themes: [String: ThemeChunk] {
        get {
            return _themes ?? [:]
        }
        set {
            _themes = newValue
        }
    }
    private var styles: [String: StyleChunk] {
        get {
            return _styles ?? [:]
        }
        set {
            _styles = newValue
        }
    }

    private var _themes: [String: ThemeChunk]?
    private var _styles: [String: StyleChunk]?

    enum CodingKeys: String, CodingKey {
        case _themes = "themes"
        case _styles = "styles"
    }

    init() {
        themes = [:]
        styles = [:]
    }

    // MARK: - Public interface
    func getThemes() -> [Theme] {
        var resultArray: [Theme] = []
        let fontDescriptorFactory = CachingFontDescriptorFactory()

        for (themeName, themeChunk) in themes {
            guard let key = ThemeKey(rawValue: themeName) else { continue }
            var styleAttributes = [String: StyleAttributes]()

            for (styleName, styleChunk) in styles {
                var fontDescriptor: UIFontDescriptor?

                var fontName = styleChunk.font ?? ""
                var fontSizeName = styleChunk.textSize ?? "0"

                if let typefaceChunk = searchTypefaces(typeface: styleChunk.typeface ?? "", theme: themeChunk) {
                    fontName = typefaceChunk.font ?? ""
                    fontSizeName = typefaceChunk.textSize ?? "0"
                }

                if let fontChunk = searchFont(font: fontName, theme: themeChunk) {
                    fontDescriptor = fontDescriptorFactory.fontDescriptor(
                        family: fontChunk.family ?? "",
                        face: fontChunk.weight ?? ""
                    )
                }

                let fontSize = searchSizes(size: fontSizeName, theme: themeChunk)

                let themeColors = getColorsDictionary(colorsRawDict: styleChunk.colors ?? [:], theme: themeChunk)

                let newStyle = StyleAttributes(
                    key: styleName,
                    colors: themeColors,
                    fontDescriptor: fontDescriptor,
                    fontSize: fontSize,
                    maximumScaleFactor: styleChunk.maximumScaleFactor
                )

                styleAttributes[styleName] = newStyle
            }

            let newTheme = Theme(key: key, isDark: themeChunk.isDark, styleAttributes: styleAttributes)
            resultArray.append(newTheme)
        }

        return resultArray
    }

    // MARK: - Private interface
    private func searchFont(font: String, theme: ThemeChunk) -> FontChunk? {
        if let result = theme.fonts?[font] {
            return result
        }

        if let parentTheme = themes[theme.parent ?? ""] {
            return searchFont(font: font, theme: parentTheme)
        }

        return nil
    }

    private func searchSizes(size: String, theme: ThemeChunk) -> CGFloat? {
        if let result = theme.sizes?[size] {
            return result
        }

        if let parentTheme = themes[theme.parent ?? ""] {
            return searchSizes(size: size, theme: parentTheme)
        }

        return nil
    }

    private func searchTypefaces(typeface: String, theme: ThemeChunk) -> TypefaceChunk? {
        if let result = theme.typefaces?[typeface] {
            return result
        }

        if let parentTheme = themes[theme.parent ?? ""] {
            return searchTypefaces(typeface: typeface, theme: parentTheme)
        }

        return nil
    }

    private func getColorsDictionary(colorsRawDict: [String: String], theme: ThemeChunk) -> [String: UIColor] {
        return Dictionary(uniqueKeysWithValues: colorsRawDict.compactMap { key, value in
            guard let color = searchColors(color: value, theme: theme) else {
                return nil
            }
            return (key, color)
        })
    }

    private func searchColors(color: String, theme: ThemeChunk) -> UIColor? {
        if let result = theme.colors?[color] {
            return UIColor(hex: result)
        }

        if let parentTheme = themes[theme.parent ?? ""] {
            return searchColors(color: color, theme: parentTheme)
        }

        return nil
    }

    static func + (lhs: DesignParser, rhs: DesignParser) -> DesignParser {
        let result = DesignParser()
        result.themes = lhs.themes
        result.styles = lhs.styles

        // Обновляем темы, которые уже есть
        for theme in rhs.themes where result.themes.keys.contains(theme.key) {
            result.themes[theme.key] = lhs.themes[theme.key]! + rhs.themes[theme.key]!
        }
        // Добавляем темы, которых нет
        for theme in rhs.themes where !result.themes.keys.contains(theme.key) {
            result.themes[theme.key] = theme.value
        }
        // Обновляем стили, которые уже есть
        for style in rhs.styles where result.styles.keys.contains(style.key) {
            result.styles[style.key] = lhs.styles[style.key]! + rhs.styles[style.key]!
        }
        // Добавляем стили, которых нет
        for style in rhs.styles where !result.styles.keys.contains(style.key) {
            result.styles[style.key] = style.value
        }

        return result
    }
}

// MARK: - Helper structs
private extension DesignParser {
    struct ThemeChunk: Decodable {
        var `default`: Bool?
        var parent: String?
        var typefaces: [String: TypefaceChunk]?
        var sizes: [String: CGFloat]?
        var fonts: [String: FontChunk]?
        var colors: [String: String]?
        var isDark: Bool

        static func + (lhs: ThemeChunk, rhs: ThemeChunk) -> ThemeChunk {
            var result = lhs
            for size in (rhs.sizes ?? [:]) where !(result.sizes ?? [:]).keys.contains(size.key) {
                result.sizes?[size.key] = size.value
            }
            for font in (rhs.fonts ?? [:]) where !(result.fonts ?? [:]).keys.contains(font.key) {
                result.fonts?[font.key] = font.value
            }
            for color in (rhs.colors ?? [:]) where !(result.colors ?? [:]).keys.contains(color.key) {
                result.colors?[color.key] = color.value
            }
            for typeface in (rhs.typefaces ?? [:]) where !(result.typefaces ?? [:]).keys.contains(typeface.key) {
                result.typefaces?[typeface.key] = typeface.value
            }
            return result
        }
    }

    struct StyleChunk: Decodable {
        var font: String?
        var textSize: String?
        var typeface: String?
        var maximumScaleFactor: CGFloat?
        var colors: [String: String]?

        static func + (lhs: StyleChunk, rhs: StyleChunk) -> StyleChunk {
            var result = lhs
            for color in (rhs.colors ?? [:]) where !(result.colors ?? [:]).keys.contains(color.key) {
                result.colors?[color.key] = color.value
            }
            return result
        }
    }

    struct FontChunk: Decodable {
        var family: String?
        var weight: String?
    }

    struct TypefaceChunk: Decodable {
        var textSize: String?
        var font: String?
    }
}

/// Кеширующая фабрика объектов UIFontDescriptor.
///
/// На большом количестве стилей приложение тратило на создание UIFontDescriptor несколько секунд,
/// а на старых устройствах вовсе падало по таймауту, не укладываясь в интервал 12 с (баг P20009346-21765).
/// Фабрика кеширует созданные объекты, чем значительно ускоряет разбор стилей и старт приложения.
private class CachingFontDescriptorFactory {
    func fontDescriptor(family: String, face: String) -> UIFontDescriptor {
        let key = Key(family: family, face: face)

        if let descriptor = cache[key] {
            return descriptor
        }

        let descriptor = fontDescriptorUncached(family: family, face: face)
        cache[key] = descriptor
        return descriptor
    }

    private func fontDescriptorUncached(family: String, face: String) -> UIFontDescriptor {
        UIFontDescriptor().withFamily(family).withFace(face)
    }

    private struct Key: Hashable {
        let family: String
        let face: String
    }

    private var cache: [Key: UIFontDescriptor] = [:]
}
