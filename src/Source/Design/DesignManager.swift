//
//  DesignManager.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 20.03.2021.
//

import UIKit

public final class DesignManager {
    // MARK: - Private props
    private var container: DesignParser
    private var themes: [Theme] = []

    // MARK: - Static interface
    public static func configureShared(withString string: String) {
        guard let newContainer = DesignManager(with: string)?.container else {
            return
        }
        let interimContainer = DesignManager.shared.container + newContainer
        DesignManager.shared.container = interimContainer
        DesignManager.shared.themes = DesignManager.shared.container.getThemes()
    }

    // MARK: - Public props
    public static var shared = DesignManager()
    public private(set) var currentTheme: Theme?

    // MARK: - Public interface
    public func set(themeKey: ThemeKey, updateViews: (() -> Void)? = nil) {
        for theme in themes where themeKey == theme.key {
            currentTheme = theme
            udpateViewsWithNewTheme()
            updateViews?()
            resetViews()
            break
        }
    }

//    public func vtbUIKitStyles() -> [URL] {
//        Bundle.module.urls(forResourcesWithExtension: "style", subdirectory: nil) ?? []
//    }

    // MARK: - Private interface
    private init() {
        container = DesignParser()
    }

    private convenience init?(with json: String) {
        guard let jsonData = json.data(using: .utf8) else {
            return nil
        }
        self.init(with: jsonData)
    }

    private func udpateViewsWithNewTheme() {
        CustomView.appearance().theme = currentTheme
//        VTBLabel.appearance().theme = currentTheme
        VTBButton.appearance().theme = currentTheme
//        VTBImageView.appearance().theme = currentTheme
//        VTBCollectionView.appearance().theme = currentTheme
//        VTBCollectionViewCell.appearance().theme = currentTheme
//        VTBCollectionReusableView.appearance().theme = currentTheme
//        VTBTextView.appearance().theme = currentTheme
//        VTBTextField.appearance().theme = currentTheme
//        VTBSwitch.appearance().theme = currentTheme
//        VTBShimmer.appearance().theme = currentTheme
//        VTBControl.appearance().theme = currentTheme
//        VTBSearchBar.appearance().theme = currentTheme
//        VTBSlider.appearance().theme = currentTheme
//        VTBProgressView.appearance().theme = currentTheme
        NavigationBar.appearance().theme = currentTheme
//        VTBTableView.appearance().theme = currentTheme
//        VTBTableViewCell.appearance().theme = currentTheme
//        VTBDatePicker.appearance().theme = currentTheme
//        VTBActivityIndicator.appearance().theme = currentTheme
//        VTBIconView.appearance().theme = currentTheme
        if #available(iOS 13.0.0, *) {
            let isDark = currentTheme?.isDark ?? false
            for window in UIApplication.shared.windows {
                window.overrideUserInterfaceStyle = isDark ? .dark : .light
                let style = DesignManager.shared.currentTheme?.style(named: "rootViewController")
                window.backgroundColor = style?.backgroundColor
            }
        }
    }

    private init?(with data: Data) {
        guard let container = try? JSONDecoder().decode(DesignParser.self, from: data) else {
            return nil
        }
        self.container = container
        themes = container.getThemes()
    }

    // этот хак-метод нужен для моментального применения изменений к вьюшкам
    private func resetViews() {
        func deepRefreshSubviews(in view: UIView) {
            for subview in view.subviews {
                if let designSystemView: DesignSystemElement = subview as? DesignSystemElement {
                    designSystemView.theme = currentTheme
                    designSystemView.refreshUI()
                }
                deepRefreshSubviews(in: subview)
            }
        }

        for window in UIApplication.shared.windows {
            for view in window.subviews {
                if let designSystemView: DesignSystemElement = view as? DesignSystemElement {
                    designSystemView.removeFromSuperview()
                    window.addSubview(designSystemView)
                }
            }
            deepRefreshSubviews(in: window)
        }
    }
}
//    import class Foundation.Bundle
//
//    private class BundleFinder {}
//
//    extension Foundation.Bundle {
//        /// Returns the resource bundle associated with the current Swift module.
//        static var module: Bundle = {
//            let bundleName = "MyStudyProject"
//
//            let candidates = [
//                // Bundle should be present here when the package is linked into an App.
//                Bundle.main.resourceURL,
//
//                // Bundle should be present here when the package is linked into a framework.
//                Bundle(for: BundleFinder.self).resourceURL,
//
//                // For command-line tools.
//                Bundle.main.bundleURL,
//            ]
//
//            for candidate in candidates {
//                let bundlePath = candidate?.appendingPathComponent(bundleName + ".bundle")
//                if let bundle = bundlePath.flatMap(Bundle.init(url:)) {
//                    return bundle
//                }
//            }
//
//            fatalError("unable to find bundle named MyStudyProject")
//        }()
//    }
