//
//  StyleAttributes.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 20.03.2021.
//

import UIKit

public struct StyleAttributes {
    private var fontDescriptor: UIFontDescriptor?
    private var fontSize: CGFloat?
    private var maximumScaleFactor: CGFloat?

    public var key: String
    public var colors: [String: UIColor]
    public var font: UIFont? {
        guard let fontDescriptor = fontDescriptor,
            let fontSize = fontSize else {
                return nil
        }
        return UIFont(descriptor: fontDescriptor, size: fontSize)
    }

    public var accessibilityFont: UIFont? {
        guard let font = self.font else {
            return nil
        }

        var scaleFactor =
            CGFloat(UIApplication.shared.preferredContentSizeCategory.scaleFactor)

        if let maximumScaleFactor = maximumScaleFactor {
            if scaleFactor > maximumScaleFactor {
                scaleFactor = maximumScaleFactor
            }
        }

        var scaledSize = font.pointSize * scaleFactor
        scaledSize.round(.down)

        return font.withSize(scaledSize)
    }

    public init(
        key: String,
        colors: [String: UIColor],
        fontDescriptor: UIFontDescriptor?,
        fontSize: CGFloat?,
        maximumScaleFactor: CGFloat?
    ) {
        self.key = key
        self.colors = colors
        self.fontDescriptor = fontDescriptor
        self.fontSize = fontSize
        self.maximumScaleFactor = maximumScaleFactor
    }
}

public extension StyleAttributes {
    var backgroundColor: UIColor? {
        return colors["backgroundColor"]
    }

    var contentViewColor: UIColor? {
        return colors["contentViewColor"]
    }

    var substrateColor: UIColor? {
        return colors["substrateColor"]
    }

    var textColor: UIColor? {
        return colors["textColor"]
    }

    var disabledColor: UIColor? {
        return colors["disabledColor"]
    }

    var tintColor: UIColor? {
        return colors["tintColor"]
    }
    var progressTintColor: UIColor? {
        return colors["progressTintColor"]
    }
    var trackTintColor: UIColor? {
        return colors["trackTintColor"]
    }
    var placeholderColor: UIColor? {
        return colors["placeholderColor"]
    }
}

public extension UIContentSizeCategory {
    var scaleFactor: Double {
        switch self {
        case .extraSmall, .small, .medium, .large, .unspecified:
            return 1
        case .extraLarge:
            return 1.1
        case .extraExtraLarge:
            return 1.2
        case .extraExtraExtraLarge, .accessibilityMedium, .accessibilityLarge,
             .accessibilityExtraLarge, .accessibilityExtraExtraLarge, .accessibilityExtraExtraExtraLarge:
            guard !UIDevice.isIPhone5 else {
                return 1.2
            }
            return 1.3
        default:
            return 1
        }
    }
}
