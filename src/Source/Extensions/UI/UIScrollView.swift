//
//  UIScrollView.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 20.03.2021.
//

import UIKit

extension UIScrollView {
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.endEditing(true)
    }
}
