//
//  UIViewController.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 20.03.2021.
//

import UIKit

extension UIViewController {
    override open func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }

    var appDelegate: AppDelegate? {
        return UIApplication.shared.delegate as? AppDelegate
    }
//    func setupAttributedText(lineHeightMultiple: Float, kern: Float, text: String, underline: Bool)
//    -> NSMutableAttributedString {
//        let paragraphStyle = NSMutableParagraphStyle()
//        paragraphStyle.lineHeightMultiple = CGFloat(lineHeightMultiple)
//        var attributes = [.kern: kern, .paragraphStyle: paragraphStyle] as [NSAttributedString.Key : Any]
//        if underline {
//            attributes[NSAttributedString.Key.underlineStyle] = NSUnderlineStyle.single.rawValue
//            return NSMutableAttributedString(string: text, attributes: attributes)
//        } else {
//            return NSMutableAttributedString(string: text, attributes: attributes)
//        }
//    }
//    func setupTitleView(label: UILabel?, text: String?, leftButton: UIButton?, rightButton: UIButton?) {
//        if let label = label {
//            label.font = UIFont(name: CustomFonts.sFUITextSemibold, size: 17)
//            let paragraphStyle = NSMutableParagraphStyle()
//            paragraphStyle.lineHeightMultiple = 1.08
//            label.attributedText = NSMutableAttributedString(string: text ?? "",
//    attributes: [NSAttributedString.Key.kern: -0.41, NSAttributedString.Key.paragraphStyle: paragraphStyle])
//            label.translatesAutoresizingMaskIntoConstraints = false
//            self.view.addSubview(label)
//
//            label.snp.makeConstraints { make in
//                make.top.equalToSuperview().offset(55)
//                make.centerX.equalToSuperview()
//            }
//        }
//        if let button = leftButton {
//            button.translatesAutoresizingMaskIntoConstraints = false
//            self.view.addSubview(button)
//
//            button.snp.makeConstraints { make in
//                make.top.equalToSuperview().offset(55)
//                make.leading.equalToSuperview().offset(15)
//            }
//        }
//        if let button = rightButton {
//            button.translatesAutoresizingMaskIntoConstraints = false
//            self.view.addSubview(button)
//
//            button.snp.makeConstraints { make in
//                make.top.equalToSuperview().offset(55)
//                make.trailing.equalToSuperview().offset(-15)
//            }
//        }
//
//    }
}
