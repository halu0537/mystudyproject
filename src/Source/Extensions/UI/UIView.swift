//
//  UIView.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 20.03.2021.
//

import UIKit

extension UIView {
//    func setupBorder(viewStyle: ViewStyle) {
//        self.layer.cornerRadius = 10
//        self.clipsToBounds = true
//        switch viewStyle {
//        case .gray:
//            self.layer.borderWidth = 1
//            self.layer.borderColor = UIColor.init(red: 0, green: 0, blue: 0, alpha: 0.1).cgColor
//        case .blue:
//            self.layer.borderWidth = 2
//            self.layer.borderColor = UIColor.init(red: 0.431, green: 0.404, blue: 0.996, alpha: 1).cgColor
//        case .red:
//            self.layer.borderWidth = 2
//            self.layer.borderColor = UIColor.red.cgColor
//        }
//    }
//    
//    enum ViewStyle {
//        case gray
//        case blue
//        case red
//    }
}
public extension UIView {
    func addPrepareSubview(_ view: UIView) {
        self.addSubview(view.prepareForAutoLayout())
    }
    @discardableResult func prepareForAutoLayout() -> Self {
        self.translatesAutoresizingMaskIntoConstraints = false
        return self
    }
}
