//
//  UIDevice.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 20.03.2021.
//

import UIKit

public extension UIDevice {
    static var isPhone: Bool {
        UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.phone
    }

    static var isIPhone5: Bool {
        isPhone && UIScreen.main.bounds.size.height == 568
    }

    static var isPad: Bool {
        UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad
    }
}
