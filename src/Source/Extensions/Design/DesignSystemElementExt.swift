//
//  DesignSystemElementExt.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 20.03.2021.
//
// swiftlint:disable type_body_length inclusive_language identifier_name file_length trailing_whitespace comment_spacing
import UIKit

enum StyleKey: String, CaseIterable {
    case textStyleHeading0
    case textStyleHeading1
    case textStyleHeading2
    case textStyleHeading3
    case textStyleHeading4
    case textStyleHeading5
    case textStyleHeading6
    case textStyleError1
    case textStyleParagraph
    case textStyleNumbers1
    case textStyleNumbers2
    case textStyleNumbers3
    case textStyleCaption1
    case textStyleCaption1emma
    case textStyleCaption2
    case textStyleCaption2gray
    case textStyleCaption3
    case textStyleMasterHeaderSecondary
    case textStyleSpasibo
    case textStyleToolbar
    case buttonStyleDefault
    case backgroundStyleDefault
    case backgroundStyleWhite
    case backgroundStyleF5
    case textStyleNumbers2Currency
    case masterFavoritesActionTitle
    case styleDiagram
    case finOverviewHideButton
    case investmentNegativeIncome
    case accountViewLargeCurrency
    case mainInvestmentCellBottomDescription
    case mainCardsAndAccountsCardView
    case mainTabBar
    case mainTabBarBadge
    case mainTabBarItem
    case mainTabOfferView
    case mainTabOfferViewHorizontal
    case mainTabOfferText
    case mainTabOfferNumber
    case savingsMonthIncome
    case accountIconWithoutChartSmallTitle
    case accountIconWithoutChartLargeTitle
    case accountIconWithChart

    case historyCellTextCard
    case historyBackgroundStyleBonus
    case historyCellBonusText
    case bonusOptionCurtainCell
    case historyCellMessageTextTitle
    case historyIconBackgroundStyle
    case historyIconInitialsText
    case historyHeaderText
    case historyWhiteBackground
    case historyF5Background
    case historyCollectionCellTitleText
    case historyEmptyViewText
    case historyEmptyViewReloadButton
    case historySearchCancelButtonStyle
    case historySearchViewStyle
    case historySearchText

    case calendarPickerFindDatesButton
    case calendarPickerLightLabel
    case calendarPickerCircleViewStyle
    case calendarPickerSecondaryFillViewStyle
    case calendarPickerMonthLabel
    case calendarPickerBorderDay
    case calendarPickerAvailableDay
    case calendarPickerUnavailableDay
    
    case showcaseBannerCell
    case showcaseBannerCellTitle
    case showcaseBannerCellDescription
    case mainSecondViewAccountCellTitle
    case mainSecondViewAccountCellAmount
    case mainSecondViewAccountCardNumber
    case mainSecondViewPager
    case mainSecondViewInvestmentPositive
    case mainSecondViewInvestmentNegative
    case mainSecondViewInvestmentImageContainer
    case mainSecondViewCellReminder
    case mainSecondViewExpandCardsTitle
    case mainSecondViewExpandCardsView
    
    case textStyleShowcaseCategoryDescription
    case textStyleShowcaseCategoryTitle
    case textStyleShowcaseCategoryTitleDisabled
    case textStyleShowcaseCategoryDescriptonHighlighed
    case textStyleShowcaseCategoryHeader
    case showcaseOffersPageIndicator
    case textStyleShowcaseOfferTitle
    case textStyleShowcaseOfferSecondary
    case textStyleShowcaseOfferHighlighted
    case textStyleShowcaseOfferDetailsDescription
    case textStyleShowcaseOfferNew
    case textStyleShowcaseApplicationTitle
    case showcaseOfferDetailsActionButton
    case showcaseOfferDetailsCancelButton
    case showcaseShutterTitle
    case showcaseShutterCloseButton
    case showcaseMarkedServiceCellTitle
    case showcaseProductBadge
    case showcaseProductDescription
    case showcaseOfferCell
    case markedServiceCell

    case minibannerCloseButton
    case minibannerTitle
    case minibannerSubtitle
    case minibannerActionButton

    case accountListField
    case textStyleAccountBalance
    case textStyleAccountName
    case textStyleAccountNumberShort

    case operationFieldTitleSelected
    case operationFieldTitleError

    case operationStatus
    case operationCheque
    case operationChequeSimple
    case operationChequeSeparator
    case operationChequeActionIcon
    case operationFieldContentSeparator

    case operationBackground
    case operationBottomViewForDetachedWidgets
    case operationField
    case operationTitleField2
    case operationGroupTitleField

    case textStyleFastPaymentTitle
    case textStyleFastPaymentSubtitle

    case newCardCellTitle

    case drawerIconDisabled
    case wwtComingSoonBadge

    case p2pPaymentTypeIcon

    case operationButtonPrimary
    case operationButtonSecondary

    case p2pContactListViewController

    case creditCardReplenishmentChip
    case creditCardReplenishmentChipAmount
    case creditCardReplenishmentChipHint
    
    case fundraisingDetailsView
    case fundraisingDetailsTitle
    case fundraisingDetailsEndDate
    case fundraisingDetailsStatus
    case fundraisingDetailsDescription
    case fundraisingDetailsNameView

    case contactCellNumberLabelDefault
    case contactCellNumberLabelDisabled
    case contactCellInitialsLabelDisabled
    case operationContactsListScreenBackground
    case operationContactsListAllowAccessTitle
    case operationContactsListAllowAccessSubtitle
    case operationContactsListAllowAccessStackButton
    case operationFieldPhonePersonIcon

    case p2pPhoneBindingGroupTitle
    case p2pPhoneBindingComment
    case p2pPhoneBindingCard

    case operationBankBicFieldBankBic
    case operationBankBicFieldBankIcon

    case shimmer
    case shimmerDarkBlue

    case investmentFundDiagram

    case noBankSelectedItemViewInOperationField
    
    case unconfirmedOperationsBannerView
    case unconfirmedOperationsBannerRoundedView
    case unconfirmedOperationsBannerCountView
    case unconfirmedOperationsButtonsView
    case unconfirmedOperationsErrorView
    case unconfirmedOperationsStatusView
    case unconfirmedOperationsConfirmButton
    case unconfirmedOperationsRejectButton
    case textStyleUnconfirmedOperationsWhiteTiny
    case textStyleUnconfirmedOperationsWhiteMedium
    case textStyleUnconfirmedOperationsBlackSmall
    case textStyleUnconfirmedOperationsGraySmall
    case textStyleUnconfirmedOperationsGrayMedium
    case textStyleUnconfirmedOperationsDate
    case textStyleUnconfirmedOperationsBlackMedium
    case textStyleUnconfirmedOperationsBlackMedium2
    case textStyleUnconfirmedOperationsEmpty
    case textStyleUnconfirmedOperationsBlackMediumRegular
    case textStyleUnconfirmedOperationsBlackBig
    
    case voipCallRootView
    case voipCallRootViewTitle
    case voipCallRootViewSubtitle
    case voipCallRootViewHideKeyboardButton
    case voipCallKeyboardViewEnteredDigits
    case voipCallKeyboardViewDigitButton
    
    case esiaAccountOpeningInitialViewControllerRootView
    case esiaAccountOpeningStatusViewControllerRootView
    case esiaAccountOpeningQuestionnaireViewControllerRootView

    case paymentsAndTransfersListViewTitle
    case paymentsAndTransfersListRowIcon
    case paymentsAndTransfersListRowIconBackgroundBlue
    case paymentsAndTransfersListRowIconBackgroundViolet
    case paymentsAndTransfersListRowIconBackgroundVioletDeep
    case paymentsAndTransfersListRowIconBackgroundFuchsia
    
    case webLoginWithQRConfirmationViewControllerRootView
    
    case flcBiometryPromotionViewControllerRootView
    case flcBiometryDocumentViewControllerRootView
    case flcBiometryAgreementInfoViewControllerRootView
    case flcBiometryVoiceRecordingViewControllerRootView
    case flcBiometryRejectionConfirmationViewControllerRootView
    
    case megabannerTitle
    case megabannerDescription
    case megabannerConditionsTitle
    case megabannerConditionsValue
    case megabannerTextBlockTitle
    case megabannerTextBlockDescription
    case megabannerActionButton
    
    case pushEnablingScreenImageBackroundView

    case summaryGroupField
    case summaryGroupFieldTitle
    
    case operationListItemCountryTitle
    
    case operationCurtainDescription

    case bankItemViewInOperationField
    case bankItemViewInOperationFieldSelected
    case bankItemViewInOperationFieldTitle
    
    case receiverCardFieldTableHeaderLabel
    ///Главный экран модуль уведомлений
    case noticeTechnicalWorksHeaderBackView
    
    case digitalProfileTitle
    case digitalProfileParagraph
    case digitalProfileDescriptionTitle
    case digitalProfileDescription
    case digitalProfileDocumentHeader
    
    case regionListCellBackground
    case listCellBackgroundSelected
    case regionListCellTitle
    case regionListSectionTitle
    case regionListNoItemsText
    case paymentsCategoryOtherRegionsButton
    case paymentsCategoryHeaderView
    case regionView
    case regionViewTitle
    case regionViewRegionButton
        
    case curtainTextScreenContentText

    case passcodeCreateError
    
    case timerLabelText
    
    //инвестиции
    case investmentHeaderTitle
    case investmentHeaderSubtitle
    case investmentSPProfitMainTitle
    case investmentSPProfitMainValue
    case investmentSPProfitSecondaryTitle
    case investmentSPProfitSecondaryValue
    case investmentSPPortfolioPromoLabel
    case investmentSPPortfolioPromoView
    case investmentConditionTitle
    case investmentConditionValue
    case paymentCalendarDateText
    case paymentCalendarPaymentTitle
    case paymentCalendarPaymentValue
    case investmentCartItemTitle
    case investmentCartItemSubtitle
    
    case closeDepositTitle
    case closeDepositProfitValueText
    case closeDepositProfitDescriptionText
    case depositsWidgetAdditionallyTitle
    case depositsWidgetCellTitle
    case depositWidgetCellAttributesTitle
    case depositWidgetProfitValueText
    case depositWidgetAttributesValueText
    case depositWidgetCellButtonTitle
    case depositWidgetTopBackground
    case depositWidgetAdditionallyTitleLabel
    case depositWidgetButtonLabel
    case depositWidgetErrorTitle
    case depositDetailsBackgroundView
    case depositDetailsStackTitles
    case depositDetailsInfoBlockBackground
    case depositDetailsInfoBlockTitle
    case depositDetailsInfoBlockSubTitle
    case depositLargeTitle
    case depositNotAvailableTitle
    case depositNotAvailableDescription
    case depositDetailsRatesTableTitle
    case depositDetailsRatesTableTermTitle
    case depositDetailsRatesTableTermSubtitle
    case depositDetailsRatesTableTermBoldTitle
    case depositDetailsRatesTableTermMediumTitle
    case depositDetailsDetailedInfoErrorTitle
    case depositDetailsOpenDepositTitle
    case depositDetailsInsuranceView
    case depositDetailsDetailedText
    case depositDetailsInsuranceText
    case depositDetailsBottomSheetView
    case depositDetailsBottomSheetTitle
    case depositDetailsBottomSheetItemTitle
    case depositDetailsBottomSheetItemSubtitle
    case depositDetailsBottomSheetSelectView
    case depositDetailsSavingsOptionView
    case depositDetailsSavingsOptionTitle
    case depositDetailsSavingsOptionSubtitle
    case depositDetailsSavingsOptionCheckboxSelected
    case depositDetailsSavingsOptionCheckboxUnselected
    case depositDetailsbaseRateDescriptionText
    case depositDetailsInfoTitle
    case depositDetailsInfoDescription
    case openDepositIncomeTitle
    case openDepositIncomeValue
    case openDepositDatePickerDoneButton
    case openDepositDatePickerTitle
    
    //product details savings
    case productDetailsAutorechargeButton
    case productDetailsAutorechargeButtonTitle
    case productDetailsAutorechargeButtonSubtitle
    case productDetailsAutorechargeScenarioImage
    case productDetailsAutorechargeScenratioTitle
    case productAutorechargeInstructionsDescription
    case productDetailsAutorechargeScenario
    
    // Аресты
    case cellArrestStatusText
    
    //investment detail carousel
    case investmentCarouselItemValue
    case investmentCarouselItemName

    // buttons in operation
    case roundedButton
    case roundedButtonDisabled

    case insuranceAlertText
    case insuranceSubtitleText
    case insuranceIcon
    
    // operation status
    case textStyleOperationStatusLabel
    
    // штрафы
    case penaltyImageViewerSelected
    case penaltyMSAImageViewerSelected
    case textStylePenaltyInputTitle

    case currentBonusOption
    case nextBonusOption
    
    //Корзина
    case investmentCartInstrumentCard
    case investmentCartInstrumentCardBlue
    case investmentCartInstrumentCardRed

    //Детальная информация
    case additionalInfoBubbleProgressSuccess
    case additionalInfoBubbleProgressFail
    case additionalInfoBubble
    case additionalInfoBubbleTitle
    case additionalInfoBubbleTitleHighlightedText
    case additionalInfoBubbleTitleHighlightedTextOrange
    case additionalInfoBubbleTitleDisabledText
    case additionalInfoBubbleDescriptionText
    case additionalInfoBubbleDescriptionHighlightedText
    case additionalInfoBubbleSubtitleText
    case additionalInfoBubbleSemiText

    //Карта возможностей
    case opportunityCardTitleLabel
    case opportunityCardDigitsLabel
    case opportunityCardSumCardLabel
    case opportunityCardNameCardLabel
    case opportunityCardNumberLabel
    case limitSlider
    case limitTextField
    case virtualCreditCardScreenTitle
    case virtualCreditCardConfirmationTitle
    case virtualCreditCardListCardTitleLight
    case virtualCreditCardListCardTitleDark
    case virtualCreditCardListCardSubtitleLight
    case virtualCreditCardListCardSubtitleDark
    case virtualCardEmissionTimeOutSubtitle
    case virtualCardInfoTitleBlack
    case virtualCardInfoTitle
    case virtualCardNameTitleBlack
    case virtualCardNameTitle
    case virtualCardBalanceTitleBlack
    case virtualCardBalanceTitle
    case virtualCardAccountBindingBlack
    case virtualCardAccountBinding
    case virtualCardNotActivatedTitle
    case virtualCreditCardSumTitle
    case virtualCreditCardTitle
    case virtualCreditCardSubtitle
    case virtualCreditCardAnswer
    case virtualCreditCardQuestion
    case virtualCreditCardBonusTitle
    case virtualCreditCardBonusSubtitle
    case virtualCardEmissionErrorSubtitle
    case virtualCardEmissionDarkBackground
    case virtualCardEmissionOnboardingFaceliftView
    
    case virtualCreditCardCashNotes
    case virtualCreditCardSectionHeader

    // Кредиты
    case lnsNavbarTextStyle
    case lnsTextInputHeaderNormal
    case lnsTextInputHeaderEditing
    case lnsTextInputHeaderError
    case lnsTextInputHeaderDisabled
    case lnsTextInputTextField
    case lnsTextInputTextFieldDisabled
    case lnsTextInputDimension
    case lnsTextInputSeparatorNormal
    case lnsTextInputSeparatorEditing
    case lnsTextInputSeparatorError
    case lnsTextInputSeparatorDisabled
    case lnsTextInputBottomHintNormal
    case lnsTextInputBottomHintEditing
    case lnsTextInputBottomHintError
    case lnsTextInputBottomHintDisabled
    case lnsCalculatorNavbarTitle
    case lnsCalculatorNavbarSubtitleNormal
    case lnsCalculatorNavbarSubtitleWarning
    case lnsSliderTitleTextInactive
    case lnsSliderTitleTextActive
    case lnsSliderInputText
    case lnsSliderInputTextDimension
    case lnsSliderDashView
    case lnsSliderBoundsText
    case lnsAddressSearchHintText
    case lnsSelectionBackground
    case lnsAccordeonControlText
    case lnsOptionListShadeCellSelected
    case lnsEarlyRepaymentMessage

    // Сканирование QR-кода
    case barcodeScannerText
    case barcodeScannerGalleryButton
    case barcodeScannerHelpButton
    case barcodeScannerPhotoScanButton
    case barcodeScannerPreviewView

    //Лэндинг Инвестиции
    case investmentLandingForexTitle
    case investmentLandingForexText
    case investmentLandingForexButton
    case paymentsSearchTableHeader
    case ofznFinancialInstrumentBackground
    case pifFinancialInstrumentBackground
    case bondFinancialInstrumentBackground
    case investVTBBondFinancialInstrumentBackground
    case depositBackground
    case savingAccountBackground
    case defaultFinancialInstrumentBackground
    case investmentAdvantagesIconGray
    case investmentSmallActionButton
    case investmentDisclaimerButton
    case investmentDisclaimerText
    case suggestionViewTitle
    case suggestionViewSubtitle

    //Шторка
    case iconCellContentViewImageBackground
    case paragraphText
    
    // Чек
    case titleMainParagraphText
    
    // банкоматы
    case atmsListIsEmptyLabel
    
    //Плашка бонусных опций на деталях продукта
    case bonusOptionCellTitleText
    case bonusOptionCellDetailText

    // Кредитный лимит
    case creditLimitCellTitle
    case creditLimitMoneyTitle
    case creditLimitMoneyLargeAmount
    case creditLimitCellContent
    case creditLimitAmountTitle
    
    // Бонусы
    case bonusOptionTitle
    case bonusOptionRewardLabel
    case bonusOptionNextRewardLabel
    case bonusOptionPurchaseLabel
    case bonusAccrualsTitle
    case bonusOptionTurnover
    
    // Нотификации
    case pushNotificationSelectedChipsView
    case pushNotificationDeselectedChipsView
    case pushNotificationSelectedChipsTitle
    case pushNotificationDeselectedChipsTitle

    // ПФМ
    case pfmNavigationBarTitle
    case pfmNavigationBarSubtitle
    case pfmTransactionsCategoryLabel
    case pfmNavigationBarLeftButton
    case pfmNavigationBarRightButton
    
    case paymentsCellHighlightedLabel

    //Автокредит
    case carLoanFormSubtitle
    
    case accountStatusLabelError
    case accountStatusLabelNormal

    //Детали транзакции
    case orderDetailsAttentionInfoTitle
    case orderDetailsAttentionInfoMessage

    // StepperView
    case stepperViewBlueContent
    
    // Перенос баланса
    case balanceTransferFAQDetail
    case balanceTransferBulletsStep
    case balanceTransferGrayNavigationBar
    case scoringWayAgreement
    case btLoansCellAmount
    case btLoansTotalTitle
    case btLoansTotalAmount
    case btRefinancingLimitInfoBackground
    
    // Карточка-шиммер
    case cardShimmerContentView
    case cardShimmerContentViewContainer
    case cardShimmerContentViewDivider

    // Оплата мобильной связи
    case mobilePaymentCommonLabel
    case mobilePaymentPhoneLabel
    case mobilePaymentSumLabel
    
    // Главный экран
    case lineDecoratedView
    
    // Детская карта
    case childrenPromoFeatureName
    case childrenPromoFeatureText
    case childrenPromoAdvantageName
    case childrenPromoAdvantageText
    case childrenCardChildInfoDatePicker
    
    // Общее
    case basicBackground
    case additionalBackground
    case selectedBackground
    case investmentCardBackground
    case shadowedView
    
    // Предложения банка
    case productDerailsBankOffersTitle
    case bankOfferCellBackground
    case bankOffersCellText
}

extension DesignSystemElement {
    var style: StyleKey? {
        get {
            return StyleKey(rawValue: styleRaw ?? "")
        }
        set {
            styleRaw = newValue?.rawValue
        }
    }

    func apply(style: StyleKey) {
        applyStyle(style: style.rawValue)
    }

    func apply<StyleKey: RawRepresentable>(style: StyleKey) where StyleKey.RawValue == String {
        applyStyle(style: style.rawValue)
    }
}

extension StyleKey {
    var attributes: StyleAttributes? {
        DesignManager.shared.currentTheme?.style(named: rawValue)
    }
    
    func color(named name: String) -> UIColor? {
        return attributes?.colors[name]
    }

    var currentAttributes: StyleAttributes? {
        DesignManager.shared.currentTheme?.style(named: rawValue)
    }
}
