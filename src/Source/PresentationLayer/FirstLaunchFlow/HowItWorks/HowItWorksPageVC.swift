//
//  HowItWorksPageVC.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 20.03.2021.
//

import UIKit

class HowItWorksPageVC: UIPageViewController {
//    var window: UIWindow?
//    var coordinator: AppCoordinator?

    private let pageControl = UIPageControl.appearance()

    private var currentViewController: HowItWorksVC?
    private var currentIndex = 0

    // MARK: - create vc
    lazy var  arrayViewController: [HowItWorksVC] = {
        var howItWorksVCs = [
            Constants.firstVC,
            Constants.secondVC,
            Constants.thirdVC,
            Constants.fourthVC
        ]
        for viewController in howItWorksVCs {
            viewController.buttonDidTap = {
                self.buttonTouchUpInside()
            }
        }
        return howItWorksVCs
    }()

    // MARK: - init UIPageViewController
    init(
        transitionStyle style: UIPageViewController.TransitionStyle,
        navigationOrientation: UIPageViewController.NavigationOrientation,
        options: [String: Any]? = nil
    ) {
        super.init(transitionStyle: style, navigationOrientation: navigationOrientation, options: nil)

        pageControl.pageIndicatorTintColor = Constants.pageIndicatorTintColor
        pageControl.currentPageIndicatorTintColor = Constants.currentPageIndicatorTintColor
        self.dataSource = self
        self.delegate = self

        if let viewController = arrayViewController.first {
            setViewControllers([viewController], direction: .forward, animated: true, completion: nil)
            self.currentViewController = viewController
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    @objc private func buttonTouchUpInside() {
        if let currentVC = self.currentViewController, let index = arrayViewController.firstIndex(of: currentVC) {
            if index < self.arrayViewController.count - 1 && index >= 0 {
                let viewController = arrayViewController[index + 1]
                self.currentViewController = viewController
                self.currentIndex = index + 1
                setViewControllers([viewController], direction: .forward, animated: true, completion: nil)
            } else if index >= 0 {
                DefaultUserService().didLaunch {
                    self.appDelegate?.changeRootVC()
                }
            }
        }
    }
}

// MARK: - UIPageViewControllerDelegate
extension HowItWorksPageVC: UIPageViewControllerDelegate {
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        return self.currentIndex
    }

    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return arrayViewController.count
    }

    func pageViewController(
        _ pageViewController: UIPageViewController,
        didFinishAnimating finished: Bool,
        previousViewControllers: [UIViewController],
        transitionCompleted completed: Bool
    ) {
        guard completed else { return }
        if
            let viewController = pageViewController.viewControllers?.first as? HowItWorksVC,
            let index = arrayViewController.firstIndex(of: viewController) {
            self.currentIndex = index
            self.currentViewController = viewController
        }
    }
}

// MARK: - UIPageViewControllerDataSource
extension HowItWorksPageVC: UIPageViewControllerDataSource {
    func pageViewController(
        _ pageViewController: UIPageViewController,
        viewControllerBefore viewController: UIViewController
    ) -> UIViewController? {
        guard let viewController = viewController as? HowItWorksVC else { return nil }
        if let index = arrayViewController.firstIndex(of: viewController),
            index > 0 {
            return arrayViewController[index - 1]
        }
        return nil
    }

    func pageViewController(
        _ pageViewController: UIPageViewController,
        viewControllerAfter viewController: UIViewController
    ) -> UIViewController? {
        guard let viewController = viewController as? HowItWorksVC else { return nil }
        if let index = arrayViewController.firstIndex(of: viewController),
            index < arrayViewController.count - 1 {
            return arrayViewController[index + 1]
        }
        return nil
    }
}
// MARK: - Constants
extension HowItWorksPageVC {
    private enum Constants {
        // VCs
        static let firstVC = HowItWorksVC(buttonTitle: "buttonTitle 1")
        static let secondVC = HowItWorksVC(buttonTitle: "buttonTitle 2")
        static let thirdVC = HowItWorksVC(buttonTitle: "buttonTitle 3")
        static let fourthVC = HowItWorksVC(buttonTitle: "buttonTitle 4")

        static let pageIndicatorTintColor = UIColor.lightGray
        static let currentPageIndicatorTintColor = UIColor(hex: "#44BF78FF")
    }
}
