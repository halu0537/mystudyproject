//
//  HowItWorksVC.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 20.03.2021.
//

import UIKit

class HowItWorksVC: ViewController {
    private lazy var button: MainButton = {
        let item = MainButton()
        item.didTap = {
            self.buttonDidTap?()
        }
        return item
    }()

    // MARK: - Callback
    public var buttonDidTap: (() -> Void)?

    init(buttonTitle: String) {
        super.init(nibName: nil, bundle: nil)
        self.createButton(self.button, title: buttonTitle)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
// MARK: - Functions

extension HowItWorksVC {
    // MARK: - UI functions
    private func createButton(_ button: MainButton, title: String) {
        button.setTitle(title)
        view.addPrepareSubview(button)
        button.snp.makeConstraints {
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }
    // MARK: - @objc functions
    // MARK: - another functions
}

// MARK: - Constants
extension HowItWorksVC {
    private enum Constants { }
}
