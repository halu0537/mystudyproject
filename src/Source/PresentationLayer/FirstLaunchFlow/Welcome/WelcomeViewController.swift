//
//  WelcomeViewController.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 20.03.2021.
//

import UIKit

class WelcomeVC: ViewController {
    private lazy var button: MainButton = {
        let item = MainButton()
        item.setTitle(Constants.buttonTitle)
        item.didTap = {
            self.pushVC()
        }
        return item
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.createButton(self.button)
    }
}

// MARK: - Functions

extension WelcomeVC {
    // MARK: - UI functions
    private func createButton(_ button: MainButton) {
        view.addPrepareSubview(button)
        button.snp.makeConstraints {
            $0.leading.trailing.bottom.equalToSuperview()
        }
    }

    // MARK: - @objc functions
    @objc private func pushVC() {
        let pageVC = HowItWorksPageVC(transitionStyle: .scroll, navigationOrientation: .horizontal, options: .none)
        self.navigationController?.pushViewController(pageVC, animated: true)
    }
    // MARK: - another functions
}

// MARK: - Constants
extension WelcomeVC {
    private enum Constants {
        static let buttonTitle = "buttonTitle"
    }
}
