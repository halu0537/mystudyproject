//
//  TabBarController.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 13.09.2021.
//

import UIKit

class TabBarController: UITabBarController {
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
    }

    private func configureUI() {
        let controller1 = MainScene.build()
        controller1.tabBarItem = UITabBarItem(tabBarSystemItem: .contacts, tag: 1)
        let nav1 = NavigationController(rootViewController: controller1)

        let controller2 = ViewController()
        controller2.tabBarItem = UITabBarItem(tabBarSystemItem: .contacts, tag: 2)
        let nav2 = NavigationController(rootViewController: controller2)

        let controller3 = ViewController()
        controller3.tabBarItem = UITabBarItem(tabBarSystemItem: .contacts, tag: 3)
        let nav3 = NavigationController(rootViewController: controller3)

        let controller4 = ViewController()
        controller4.tabBarItem = UITabBarItem(tabBarSystemItem: .contacts, tag: 4)
        let nav4 = NavigationController(rootViewController: controller4)

        let controller5 = ViewController()
        controller5.tabBarItem = UITabBarItem(tabBarSystemItem: .contacts, tag: 5)
        let nav5 = NavigationController(rootViewController: controller5)

        viewControllers = [nav1, nav2, nav3, nav4, nav5]
    }
}
