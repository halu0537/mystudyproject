//
//  MainScene.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 13.09.2021.
//

import UIKit

enum MainScene {
    static func build() -> UIViewController {
        let interactor = MainInteractor()
        let presenter = MainPresenter()
        let viewController = MainViewController()
        let router = MainRouter()
        let worker = MainWorker()

        interactor.presenter = presenter
        interactor.worker = worker

        presenter.viewController = viewController

        viewController.interactor = interactor
        viewController.router = router

        router.viewController = viewController

        return viewController
    }
}
