//
//  MainViewController.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 13.09.2021.
//

import Foundation
import UIKit

final class MainViewController: ViewController, MainDisplayLogic {
    // MARK: - Internal properties
    var interactor: MainBusinessLogic?
    var router: MainRoutingLogic?

    // MARK: - Private properties
    private let button = UIButton()

    // MARK: - View lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureUI()
        interactor?.loadData(with: MainModel.Data.Request())
    }

    // MARK: - Private methods
    private func configureUI() {
        button.backgroundColor = .red
        button.addTarget(self, action: #selector(didTap), for: .touchUpInside)
        view.addSubview(button.prepareForAutoLayout())
        button.snp.makeConstraints {
            $0.width.equalToSuperview().offset(-50)
            $0.height.equalTo(50)
            $0.center.equalToSuperview()
        }
    }

    @objc private func didTap() {
        router?.routeToNewScene()
    }

    // MARK: - MainDisplayLogic
    func displayDisplay(with response: Model.Data.ViewModel) {
        title = response.title
        button.setTitle(response.buttonTitle, for: .normal)
    }
}
