//
//  MainInteractor.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 13.09.2021.
//

final class MainInteractor: MainBusinessLogic {
    // MARK: - Internal properties
    var presenter: MainPresentationLogic?
    var worker: MainWorkingLogic?

    // MARK: - MainBusinessLogic
    func loadData(with request: Model.Data.Request) {
        worker?.downloadData { [weak presenter] response in
            presenter?.presentData(with: response)
        }
    }
}
