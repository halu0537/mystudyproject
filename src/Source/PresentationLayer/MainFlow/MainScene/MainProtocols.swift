//
//  MainProtocols.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 13.09.2021.
//

protocol MainBusinessLogic: AnyObject {
    typealias Model = MainModel

    func loadData(with request: Model.Data.Request)
}

protocol MainDisplayLogic: AnyObject {
    typealias Model = MainModel

    func displayDisplay(with response: Model.Data.ViewModel)
}

protocol MainPresentationLogic: AnyObject {
    typealias Model = MainModel

    func presentData(with response: Model.Data.Response)
}

protocol MainRoutingLogic {
    func routeToNewScene()
}

protocol MainWorkingLogic {
    typealias Model = MainModel

    func downloadData(completion: @escaping (Model.Data.Response) -> Void)
}
