//
//  MainPresenter.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 13.09.2021.
//

final class MainPresenter: MainPresentationLogic {
    // MARK: - Internal properties
    weak var viewController: MainDisplayLogic?

    // MARK: - MainPresentationLogic
    func presentData(with response: Model.Data.Response) {
        viewController?.displayDisplay(
            with: Model.Data.ViewModel(
                title: response.title,
                buttonTitle: response.buttonTitle
            )
        )
    }
}
