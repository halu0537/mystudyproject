//
//  MainWorker.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 13.09.2021.
//

final class MainWorker: MainWorkingLogic {
    // MARK: - MainWorkingLogic
    func downloadData(completion: @escaping (Model.Data.Response) -> Void) {
        completion(
            Model.Data.Response(
                title: "title",
                buttonTitle: "button title"
            )
        )
    }
}
