//
//  MainModel.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 13.09.2021.
//

enum MainModel {
    enum Data {
        struct Request { }
        struct Response {
            let title: String
            let buttonTitle: String
        }
        struct ViewModel {
            let title: String
            let buttonTitle: String
        }
    }
}
