//
//  MainRouter.swift
//  MyStudyProject
//
//  Created by Радим Гасанов on 13.09.2021.
//

import UIKit

final class MainRouter: MainRoutingLogic {
    // MARK: - Internal properties
    weak var viewController: UIViewController?

    // MARK: - MainRoutingLogic
    func routeToNewScene() {
        viewController?.navigationController?.pushViewController(ViewController(), animated: true)
    }
}
